scalaVersion := "3.3.1"
libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "2.10.0"
)
libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "ch.qos.logback" % "logback-classic" % "1.3.5"
)
