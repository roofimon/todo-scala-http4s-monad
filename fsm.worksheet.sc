// Define the states of the finite state machine
sealed trait State
case object StateA extends State
case object StateB extends State
case object StateC extends State

// Define the events that trigger state transitions
sealed trait Event
case object Event1 extends Event
case object Event2 extends Event
case object Event3 extends Event

// Define the finite state machine class
class FiniteStateMachine {
  private var currentState: State = StateA

  // Define the transitions between states based on events
  def transition(event: Event): Unit = {
    currentState = (currentState, event) match {
      case (StateA, Event1) => StateB
      case (StateB, Event2) => StateC
      case (StateC, Event3) => StateA
      case _                => currentState // No transition for other cases
    }
  }

  // Get the current state of the finite state machine
  def getCurrentState: State = currentState
}

// object Main extends App {
// Create an instance of the finite state machine
val fsm = new FiniteStateMachine()

// Perform state transitions based on events
fsm.transition(Event1)
println(s"Current State: ${fsm.getCurrentState}")

fsm.transition(Event2)
println(s"Current State: ${fsm.getCurrentState}")

fsm.transition(Event3)
println(s"Current State: ${fsm.getCurrentState}")
// }
