import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer

sealed trait State
case object Todo extends State
case object Doing extends State
case object Done extends State

sealed trait Event
case object Forwarded extends Event
case object Backwarded extends Event
case object Reseted extends Event
case class Assigned(owner: Owner) extends  Event

sealed trait Owner
case class People(name: String) extends Owner
case object NoOne extends Owner

case class Task(title: String, currentState: State, owner: Owner)

object Task:
  def transition(todo: Task, event: Event): Task = 
    (todo.currentState, event) match
        case (Todo, Forwarded)  => todo.copy(currentState = Doing)
        case (Doing, Forwarded) => todo.copy(currentState = Done)
        case (Done, Forwarded)  => todo
        case (Todo, Backwarded) => todo
        case (Doing, Backwarded) => todo.copy(currentState = Todo)
        case (Done, Backwarded)  => todo.copy(currentState = Doing)
        case (_, Reseted) => todo.copy(currentState = Todo)
        case (_, Assigned(assignee)) => todo.copy(owner = assignee)


sealed trait Repository:
    def findByTitle(target: String): Option[Task] 
    def removeTask(task: Task): Option[Task] 
    def addTask(task: Task): Option[ArrayBuffer[Task]] 

case class ArrayBufferRepository(boardState: State, arrayBuffer: ArrayBuffer[Task]) extends Repository:
    def findByTitle(target: String): Option[Task] = 
        arrayBuffer.filter(task => task.title == target).headOption

    def removeTask(task: Task): Option[Task] = 
        val targetIndex = arrayBuffer.indexOf(task)
        if (targetIndex >= 0)
            todoList.remove(targetIndex) 
            Some(task)
        else
            None
    def addTask(task: Task): Option[ArrayBuffer[Task]] = 
        task.currentState == this.boardState match
            case true => Some(arrayBuffer += (task))
            case false => None         

case class TodoService(todoRepo: Repository, doingRepo: Repository):
    def moveFromTodoToDoing(target: String): Option[String] = 
        oneUnitOfWork(
            taskTitle= target, 
            source=todoRepo, 
            target=doingRepo, 
            event=Forwarded
        ) match
            case Some(doingList) => 
                Some(s"Successfully moved task- ${doingList.last.title} -> ${doingList.last.currentState}")
            case None => 
                None
    
    def assignToAssignee(target: Task, assignee: Owner) = 
        val repository = target.currentState match
            case Todo => todoRepo
            case Doing => doingRepo  
            case Done => doneRepo
        oneUnitOfWork(
            taskTitle = target.title, 
            source= repository, 
            target = repository, 
            event=Assigned(som)
        ) match
            case Some(value) => Some(s"Successfully assigned task to ${assignee}")
            case None => None

    def oneUnitOfWork(taskTitle: String, source: Repository, target: Repository, event: Event): Option[ArrayBuffer[Task]] = 
        source.findByTitle(taskTitle)
            .flatMap(task => source.removeTask(task))
            .flatMap(task => Some(Task.transition(task, event)))
            .flatMap(task => target.addTask(task))

//Main Program
val som = People("Som")
val jame = People("jame")
var todoList: ArrayBuffer[Task] = scala.collection.mutable.ArrayBuffer.empty[Task]
var doingList: ArrayBuffer[Task] = scala.collection.mutable.ArrayBuffer.empty[Task]
var doneList: ArrayBuffer[Task] = scala.collection.mutable.ArrayBuffer.empty[Task]
val taskA= Task("a", Todo, NoOne)
val taskB = Task("b", Todo, NoOne)
val taskC = Task("c", Todo, NoOne)
todoList += taskA
todoList +=taskB
todoList +=taskC

val todoRepo = ArrayBufferRepository(Todo, todoList)
val doingRepo = ArrayBufferRepository(Doing, doingList)
val doneRepo = ArrayBufferRepository(Done, doneList)
val service = TodoService(todoRepo, doingRepo)

service.assignToAssignee(taskA, som) match
    case Some(message) => println(message)
    case None => println("Fail to assign")

Some("a").flatMap(service.moveFromTodoToDoing) match
    case Some(message) => println(s"${message}")
    case None => println("Nothing to be moved")

todoList
doingList