import java.util.{Date, UUID}

def generateCustomUuid(date: Date, a: Int, b: Int): UUID = {
    val timestamp = date.getTime
    val combinedValue = (a.toLong << 31) + b.toLong
    new UUID(timestamp, combinedValue)
}

val currentDate = new Date()
val customUuid = generateCustomUuid(currentDate, 223, 43532)

println(customUuid)