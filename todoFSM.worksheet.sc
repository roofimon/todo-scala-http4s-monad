sealed trait State
case object Todo extends State
case object Doing extends State
case object Done extends State

sealed trait Event
case object Forwarded extends Event
case object Backwarded extends Event
case object Reseted extends Event

case class Task(title: String, currentState: State)

object Task:
  def transition(todo: Task, event: Event): Task = 
    (todo.currentState, event) match
        case (Todo, Forwarded) => todo.copy(currentState = Doing)
        case (Doing, Forwarded) => todo.copy(currentState = Done)
        case (Done, Forwarded) => todo
        case (Todo, Backwarded) => todo
        case (Doing, Backwarded) => todo.copy(currentState = Todo)
        case (Done, Backwarded) => todo.copy(currentState = Doing)
        case (_, Reseted) => todo.copy(currentState = Todo)

val todoList: List[Task] = List(Task("a", Todo), Task("b", Todo))
val doingList: List[Task] = List()

case class TodoRepository(todoList: List[Task]):
    def findByTitle(target: String): Option[Task] = 
        todoList.filter(task => task.title == target).headOption

    def removeTask(task: Task): List[Task] = 
        todoList.filterNot(_==task)


case class DoingRepository(doingList: List[Task]):
    def findByTitle(target: String): Option[Task] = 
        doingList.filter(task => task.title == target).headOption
    
    def addTask(task: Task): List[Task] = 
        doingList.appended(task)

val todoRepo = TodoRepository(todoList)
val doingRepo = DoingRepository(doingList)

case class TodoService(todoRepo: TodoRepository, doingRepo: DoingRepository):
    def moveFromTodoToDoing(target: String): Option[String] = 
        todoRepo.findByTitle(target) match
            case Some(task) => {
                todoRepo.removeTask(task)
                doingRepo.addTask(task)
                Some(s"Successfully moved ${task.title} to doing")
            }
            case None => None


val service = TodoService(todoRepo, doingRepo)

Some("a").flatMap(service.moveFromTodoToDoing) match
    case Some(message) => println(s"${message}")
    case None => println("Nothing to be moved")
