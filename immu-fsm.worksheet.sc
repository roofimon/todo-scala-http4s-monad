// Define the states of the finite state machine
sealed trait State
case object StateA extends State
case object StateB extends State
case object StateC extends State

// Define the events that trigger state transitions
sealed trait Event
case object Event1 extends Event
case object Event2 extends Event
case object Event3 extends Event

case class FiniteStateMachine(currentState: State)

// Define the immutable finite state machine class
object FiniteStateMachine {
  // Define the transitions between states based on events
  def transition(
      machine: FiniteStateMachine,
      event: Event
  ): FiniteStateMachine =
    (machine.currentState, event) match {
      case (StateA, Event1) => machine.copy(currentState = StateB)
      case (StateB, Event2) => machine.copy(currentState = StateC)
      case (StateC, Event3) => machine.copy(currentState = StateA)
      case _                => machine // No transition for other cases
    }
}

// Create an instance of the immutable finite state machine
val fsmInitialState = FiniteStateMachine(StateA)

// Perform state transitions based on events
val fsmAfterEvent1 = FiniteStateMachine.transition(fsmInitialState, Event1)
println(s"Current State: ${fsmAfterEvent1.currentState}")

val fsmAfterEvent2 =
  FiniteStateMachine.transition(fsmAfterEvent1, Event2)
println(s"Current State: ${fsmAfterEvent2.currentState}")

val fsmAfterEvent3 =
  FiniteStateMachine.transition(fsmAfterEvent2, Event3)
println(s"Current State: ${fsmAfterEvent3.currentState}")
