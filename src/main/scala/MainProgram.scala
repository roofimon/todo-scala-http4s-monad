import scala.collection.mutable.ArrayBuffer
import todo.model.*
import todo.ArrayBufferRepository
import todo.TodoService
import com.typesafe.scalalogging.Logger

object MainProgram extends App, Intializer:
  val logger = Logger("MainProgram")
  service.createTaskInTodo() match
    case Right(newTask) => 
      service.assignOwner(newTask, People("Som")) match
        case Right(message) => logger.debug(message)
        case Left(message)  => logger.debug(s"Fail to assign because ${message}")

      Right(newTask.id).flatMap(service.moveTaskToDoing) match
        case Right(message) => logger.debug(message)
        case Left(message)  => logger.debug(s"Fail to move task because ${message}")

      Right(newTask.id).flatMap(service.moveTaskToDone) match
        case Right(message) => logger.debug(message)
        case Left(message)  => logger.debug(s"Fail to move task because $message")

    case Left(exeption) => println("xxx")
