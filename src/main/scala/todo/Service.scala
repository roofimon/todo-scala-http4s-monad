package todo
import todo.model.*
import scala.collection.mutable.ArrayBuffer
import com.typesafe.scalalogging.Logger
import java.util.{Date, UUID}

def generateCustomUuid(date: Date, a: Int, b: Int): UUID = {
    val timestamp = date.getTime
    val combinedValue = (a.toLong << 31) + b.toLong
    new UUID(timestamp, combinedValue)
}

case class TodoService(
    todoRepo: Repository,
    doingRepo: Repository,
    doneRepo: Repository
):
  val logger = Logger("TodoService")
  def createTaskInTodo():Either[String, Task]= 
      val newId = generateCustomUuid(new Date(), 1, 1).toString()
      val taskA = Task(newId,None, Todo, NoOne)
      todoRepo.addTask(taskA) match
        case Right(todoList) => 
          logger.info(s"TaskCreatedInTodo- $taskA")
          Right(taskA)
        case Left(exception) => Left(s"Fail to create new task in Todo")
      
  def moveTaskToDoing(target: String): Either[String, String] =
    oneUnitOfWork(
      taskTitle = target,
      source = todoRepo,
      target = doingRepo,
      event = Forwarded
    ) match
      case Right(doingList) =>
        logger.info(s"TaskMovedToDoing - $target")
        Right(
          s"Successfully moved task- ${doingList.last.title} -> ${doingList.last.currentState}"
        )
      case Left(exception) => Left(exception.getMessage)

  def moveTaskToDone(target: String): Either[String, String] =
    oneUnitOfWork(
      taskTitle = target,
      source = doingRepo,
      target = doneRepo,
      event = Forwarded
    ) match
      case Right(doneList) =>
        logger.info(s"TaskMovedToDone - $target")
        Right(
          s"Successfully moved task- ${doneList.last.title} -> ${doneList.last.currentState}"
        )
      case Left(exception) => Left(exception.getMessage)

  def assignOwner(target: Task, assignee: Owner): Either[String, String] =
    val repository = target.currentState match
      case Todo  => todoRepo
      case Doing => doingRepo
      case Done  => doneRepo
    oneUnitOfWork(
      taskTitle = target.id,
      source = repository,
      target = repository,
      event = Assigned(assignee)
    ) match
      case Right(value) =>
        logger.info(s"OwnerAssigned - $target")
        Right(s"Successfully assigned task to ${assignee}")
      case Left(exception) => Left(exception.getMessage)

  def oneUnitOfWork(
      taskTitle: String,
      source: Repository,
      target: Repository,
      event: Event
  ) =
    source
      .findById(taskTitle)
      .flatMap(task => source.removeTask(task))
      .flatMap(task => Right(Task.transition(task, event)))
      .flatMap(task => target.addTask(task))
