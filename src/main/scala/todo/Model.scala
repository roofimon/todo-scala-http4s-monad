package todo.model

sealed trait State
case object Todo extends State
case object Doing extends State
case object Done extends State

sealed trait Event
case object Forwarded extends Event
case object Backwarded extends Event
case object Reseted extends Event
case class Assigned(owner: Owner) extends Event

sealed trait Owner
case class People(name: String) extends Owner
case object NoOne extends Owner

case class Task(id: String, title: Option[String], currentState: State, owner: Owner)

object Task:
  def transition(todo: Task, event: Event): Task = 
    (todo.currentState, event) match
        case (Todo, Forwarded)  => todo.copy(currentState = Doing)
        case (Doing, Forwarded) => todo.copy(currentState = Done)
        case (Done, Forwarded)  => todo
        case (Todo, Backwarded) => todo
        case (Doing, Backwarded) => todo.copy(currentState = Todo)
        case (Done, Backwarded)  => todo.copy(currentState = Doing)
        case (_, Reseted) => todo.copy(currentState = Todo)
        case (_, Assigned(assignee)) => todo.copy(owner = assignee)