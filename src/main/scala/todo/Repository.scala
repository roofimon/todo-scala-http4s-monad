package todo
import todo.model.*
import scala.collection.mutable.ArrayBuffer
import cats.syntax.either._

sealed trait Repository:
  def findById(target: String): Either[Exception, Task]
  //def findByTitle(target: String): Either[Exception, Task]
  def removeTask(task: Task): Either[Exception, Task]
  def addTask(task: Task): Either[Exception, ArrayBuffer[Task]]

case class ArrayBufferRepository(
    boardState: State,
    arrayBuffer: ArrayBuffer[Task]
) extends Repository:
  def findById(target: String): Either[Exception, Task] = 
    val index = arrayBuffer.indexWhere(task => task.id == target)
    index >= 0 match
      case true => Right(arrayBuffer.apply(index))
      case false =>
        Left(new Exception("Could find task that match with criteria"))

  /* def findByTitle(target: String): Either[Exception, Task] =
    val index = arrayBuffer.indexWhere(task => task.title == target)
    index >= 0 match
      case true => Right(arrayBuffer.apply(index))
      case false =>
        Left(new Exception("Could find task that match with criteria"))
 */
  def removeTask(task: Task): Either[Exception, Task] =
    val targetIndex = arrayBuffer.indexOf(task)
    targetIndex >= 0 match
      case true => Right(arrayBuffer.remove(targetIndex))
      case false =>
        Left(new Exception(s"Fail to remove task from ${boardState}"))

  def addTask(task: Task): Either[Exception, ArrayBuffer[Task]] =
    task.currentState == this.boardState match
      case true => Right(arrayBuffer += (task))
      case false =>
        Left(
          new Exception(s"Not allow to ${task.currentState} to ${boardState}")
        )
