import scala.collection.mutable.ArrayBuffer
import java.util.{Date, UUID}

def generateCustomUuid(date: Date, a: Int, b: Int): UUID = {
  val timestamp = date.getTime
  val combinedValue = (a.toLong << 31) + b.toLong
  new UUID(timestamp, combinedValue)
}

val currentDate = new Date()
val customUuid = generateCustomUuid(currentDate, 23, 43532)

trait Intializer {
  import todo.model._
  import todo._
  var todoList: ArrayBuffer[Task] =
    scala.collection.mutable.ArrayBuffer.empty[Task]
  var doingList: ArrayBuffer[Task] =
    scala.collection.mutable.ArrayBuffer.empty[Task]
  var doneList: ArrayBuffer[Task] =
    scala.collection.mutable.ArrayBuffer.empty[Task]
  val taskA = Task("1234", Some("Buy banana"), Todo, NoOne)
  /* val taskB = Task("1324", "Submit proposal", Todo, NoOne)
  val taskC = Task("1432", "Create new quotation", Todo, NoOne)
  val taskD = Task("4321", "Deliver UPS to office", Todo, NoOne)
   */
  todoList += taskA
  /* todoList += taskB
  todoList += taskC
  todoList += taskD */

  val todoRepo = ArrayBufferRepository(Todo, todoList)
  val doingRepo = ArrayBufferRepository(Doing, doingList)
  val doneRepo = ArrayBufferRepository(Done, doneList)
  val service = TodoService(todoRepo, doingRepo, doneRepo)
}
